#!/bin/bash
#
# Setup the the box. This runs as root

apt-get -y update
apt-get -y dist-upgrade

apt-get -y install \
    bridge-utils \
    curl \
    ebtables \
    git \
    htop \
    screen \
    software-properties-common \
    vim-nox \
    $(NULL)


# You can install anything you need here.
